def bintree(n):
    size = range(0,n)
    if n==0:
        yield 'green'
        yield 'red'
        yield 'blue'
    else:
        for a in size:
            for b in bintree(a):
                for c in bintree(n-1-a):
                    yield (b,c)


for i in bintree(2):
    print(i)
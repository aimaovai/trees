male = ("Tywin","Jaimie","Tyrion","Joffrey","Tommen","Robert","Stannis","Renly","Steffon","Aegon V","Ormund","Tytos","Maekar I")
female = ("Cersei","Myrcella","Shireen","Margery","Joanna","Rhaelle")

parents = {
    "Joffrey":["Robert","Cersei"],
    "Tommen":["Robert","Cersei"],
    "Myrcella":["Robert","Cersei"],
    "Shireen":"Stannis",
    "Tyrion":["Tywin","Joanna"],
    "Jaimie":["Tywin","Joanna"],
    "Cersei":["Tywin","Joanna"],
    "Robert":"Steffon",
    "Stannis":"Steffon",
    "Renly":"Steffon",
    "Steffon":["Rhaelle","Ormund"],
    "Rhaelle":"Aegon V",
    "Aegon V":"Maekar I"
}
#print(parents["Joffrey"])
def parent_of(person):
    print(parents[person])

#parent_of("Jeffrey")

def sibling_of(person):
    for i in parents:
        if parents[person] == parents[i]:
            #sibling = i
            print(i)

#sibling_of("Joffrey")

def brother_of(person):
    for i in parents:
        if parents[person] == parents[i] and i in male and i != person:
            print(i)

#brother_of("Joffrey")

def sister_of(person):
    for i in parents:
        if parents[person] == parents[i] and i in female and i != person:
            print(i)

#sister_of("Joffrey")

def mother_of(person):
    if parents[person] in female:
        print(parents[person])

# mother_of("Joffrey")
def ancestor_of(person):
    return parent_of(person)

def ancestor_of(person):
    if person in parents:
        p = parents[person]
        for i in p:
            ancestor_of(i)
            print(i)